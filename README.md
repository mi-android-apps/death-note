# deathNote
Death Note Is a manga in which Light Yagami is the owner of a notebook of death. 
The person whose name is written in this book dies inevitably.

### Objectives
* Learn how to use SQLite, the database system that runs on android
* Learn the fundamentals of working with a content provider
* loading data

### Build Instructions
This sample uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

### screenshots
![please find images under app-screenshots directory](app-screenshots/screen-1.png "screen one")
![please find images under app-screenshots directory](app-screenshots/screen-2.png "screen two")
![please find images under app-screenshots directory](app-screenshots/screen-3.png "screen three")
![please find images under app-screenshots directory](app-screenshots/screen-4.png "screen four")
![please find images under app-screenshots directory](app-screenshots/screen-5.png "screen five")
![please find images under app-screenshots directory](app-screenshots/screen-6.png "screen six")
