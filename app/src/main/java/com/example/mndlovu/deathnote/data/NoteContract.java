package com.example.mndlovu.deathnote.data;

import android.net.Uri;
import android.provider.BaseColumns;

public class NoteContract {

    public final static String CONTENT_AUTHORITY = "com.example.mndlovu.deathnote";
    public final static Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public final static String PATH_NOTES = "notes";


    private NoteContract() {}

    public final static class NoteEntry implements BaseColumns {

        public final static Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_NOTES);
        public final static String TABLE_NAME = "notes";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_NAME = "name";
        public final static String COLUMN_DESCRIPTION = "description";
        public final static String COLUMN_DATETIME = "datetime";
    }
}
