package com.example.mndlovu.deathnote;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.mndlovu.deathnote.data.NoteContract.NoteEntry;
import com.example.mndlovu.deathnote.data.NoteDBHelper;

import java.util.Calendar;

public class AddDetailActivity extends AppCompatActivity {

    private String date = "";
    private String time = "";
    private NoteDBHelper noteDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_detail);
        noteDBHelper = new NoteDBHelper(this);
    }

    private void insertANote(String name, String description, String date) {
        SQLiteDatabase db = noteDBHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NoteEntry.COLUMN_NAME, name);
        values.put(NoteEntry.COLUMN_DESCRIPTION, description);
        values.put(NoteEntry.COLUMN_DATETIME, date);

        long newRowId = db.insert(NoteEntry.TABLE_NAME, null, values);
        Toast.makeText(this, "number of rows : " + newRowId, Toast.LENGTH_SHORT).show();
    }

    public void addNoteToTheList() {
        EditText name = findViewById(R.id.name_editText);
        EditText description = findViewById(R.id.description_editText);
        if (name.getText().toString().isEmpty()) {
            Toast.makeText(this,"name field is empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (description.getText().toString().isEmpty()) {
            Toast.makeText(this,"description field is empty", Toast.LENGTH_LONG).show();
            return;
        }

        if (date.isEmpty()) {
            Toast.makeText(this,"please select a date", Toast.LENGTH_LONG).show();
            return;
        }

        if (time.isEmpty()) {
            Toast.makeText(this,"please select a time", Toast.LENGTH_LONG).show();
            return;
        }

        insertANote(name.getText().toString(),  description.getText().toString(), date + " " + time);
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                addNoteToTheList();
                return true;
            case android.R.id.home:
                // Navigate back to parent activity (MainActivity)
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getDate(View view) {
        int mYear, mMonth, mDay;
        final Button button = (Button) view;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                        button.setText(date);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void getTime(View view) {
        int  mHour, mMinute;
        final Button button = (Button) view;
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        time = hourOfDay + ":" + minute;
                        button.setText(time);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }
}
