package com.example.mndlovu.deathnote;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.mndlovu.deathnote.data.NoteContract.NoteEntry;

public class NoteCursorAdapter extends CursorAdapter {

    public NoteCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_items, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView name = view.findViewById(R.id.name_text_view);
        TextView description = view.findViewById(R.id.description_text_view);
        TextView date = view.findViewById(R.id.date_text_view);

        int nameColumnIndex = cursor.getColumnIndex(NoteEntry.COLUMN_NAME);
        int descriptionColumnIndex = cursor.getColumnIndex(NoteEntry.COLUMN_DESCRIPTION);
        int dateColumnIndex = cursor.getColumnIndex(NoteEntry.COLUMN_DATETIME);

        String mName = cursor.getString(nameColumnIndex);
        String mDescription = cursor.getString(descriptionColumnIndex);
        String mDate = cursor.getString(dateColumnIndex);

        name.setText(mName);
        description.setText(mDescription);
        date.setText(mDate);
    }
}
